resource "aws_efs_file_system" "wordpress_content" {
  creation_token = "A4L-WORDPRESS-CONTENT"
  performance_mode = "generalPurpose"
  throughput_mode  = "bursting"
  encrypted       = false  # Set to true for production use

  lifecycle_policy {
    transition_to_ia = "AFTER_30_DAYS"
  }

  tags = {
    Name = "A4L-WORDPRESS-CONTENT"
  }
}

resource "aws_efs_mount_target" "efs_mount_target_a" {
  file_system_id = aws_efs_file_system.wordpress_content.id
  #availability_zone_name = "us-east-1a"
  subnet_id      = "subnet-0e6b310a77b8a6e1c"
  security_groups = ["sg-09e2edcccb8095d62"]
}

resource "aws_efs_mount_target" "efs_mount_target_b" {
  file_system_id = aws_efs_file_system.wordpress_content.id
  #availability_zone_name = "us-east-1b"
  subnet_id      = "subnet-0c012de365fb83478"
  security_groups = ["sg-09e2edcccb8095d62"]
}

resource "aws_efs_mount_target" "efs_mount_target_c" {
  file_system_id = aws_efs_file_system.wordpress_content.id
  #availability_zone_name = "us-east-1c"
  subnet_id      = "subnet-036b762e853087b4f"
  security_groups = ["sg-09e2edcccb8095d62"]
}