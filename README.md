## Creating three tiers with Cloudformation
This CloudFormation template is designed to create a secure and scalable infrastructure with different tiers (public, private, and database) within a VPC. The template defines various AWS resources, such as the VPC itself, subnets, route tables, security groups, an internet gateway, and more. 
 1. ### VPC and Internet Gateway:

VPC with a specific CIDR block, DNS support, and DNS hostnames enabled.
Attaches an Internet Gateway to the VPC for internet access.


 2. ### Subnets:

Created multiple subnets for different availability zones, each with its CIDR block and IPv6 CIDR block. 

 3. ### Route Tables:

Created a public route table (RTPub) associated with the internet gateway. 
Adds default routes for both IPv4 and IPv6 to the internet gateway. Associates subnets with the public route table.

# Terraform

## Provisioning the EC2 Instance, ALB and ASG in the public subnet, Provisioning RDS, EFS in the private subnet

 1. Created a new file named main.tf and contains - terraform block contains Terraform settings and the provider block configures the aws provider. A provider is a plugin that Terraform uses to create and manage your resources.

 2. Created three modules namely RDS, EC2 and PARAMETERS. All the mentioned modules have the directory and their name is same as the module name. All of them have main.tf file

 3. ### RDS module. 
 
 Created two resources - db subnet group and db instance. For simplicity, I have hardcoded the username and password. Checked with Terraform init, plan and apply command to make sure the instance is provisioning at the right subnet and specified instance configuration. Reference - [db_instance](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/db_instance)
 ```
 resource "aws_db_subnet_group" "wordpress_rds_subnet_group" {
  name        = "wbrdssubnetgroup"
  description = "RDS Subnet Group for WordPress"
 # vpc_id      = var.aws_vpc_id
  subnet_ids = [
    "subnet-0da5160242404f6c5",
    "subnet-010af119830de319c",
    "subnet-09502176638ef4691"
  ]
}

resource "aws_db_instance" "wordpress_rds_instance" {
  identifier           = "a4lwordpress"
  engine               = "mysql"
  engine_version       = "8.0.32"
  instance_class       = "db.t2.micro"  # Choose appropriate instance class
  allocated_storage    = 20  # Size in GB
  storage_type         = "gp2"
  publicly_accessible  = false

  vpc_security_group_ids = ["sg-05113bf6d16c0599b"]  # Replace with your actual security group ID

  db_subnet_group_name  = aws_db_subnet_group.wordpress_rds_subnet_group.name
  multi_az              = false
  availability_zone     = "us-east-1a"
#  initial_databse_name  = "a4lwordpressdb"
  username              = "a4lwordpressuser"
  password              = "4n1m4l54L1f3"
  skip_final_snapshot = true
}
 ```
![](https://lh7-us.googleusercontent.com/C9C50CGRZTV0Yk5XRtKeFpKjEIOFhQjC1qqAZNgPJ7ciYdj3ZYZl90O8euFFyreV-wm-SV31GjIThr1al0FBDqs7z2sA0C1mwTVBHUX5U2qoOoJkGAkfc2L1_2S5wJ470shL4KBpUattHJeLfcQOR6U)

![](https://lh7-us.googleusercontent.com/FCqgqpP8DNDEQ0maPDIHv_tS4icZjxSbJ0oKuhEgAqfaIC7xAV7Gj0TaZDYPgHr8cUj4T059AKe0VdAAXkOzp8b4pYt3FRh_Btv9RopLVZ00BDjh6xLr8AZLHkP1iSYlL9lYcSMgXnv3seLP68-4y84)


 4. ### EFS module. 
 
 created resources - aws_efs_file_system. Ran Terraform init, plan and apply command to make sure the file system is provisioned at the right subnet and specified configuration. Reference - [efs_file_system](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/efs_file_system#using-lifecycle-policy)
```
resource "aws_efs_file_system" "wordpress_content" {
  creation_token = "A4L-WORDPRESS-CONTENT"
  performance_mode = "generalPurpose"
  throughput_mode  = "bursting"
  encrypted       = false  # Set to true for production use

  lifecycle_policy {
    transition_to_ia = "AFTER_30_DAYS"
  }

  tags = {
    Name = "A4L-WORDPRESS-CONTENT"
  }
}

resource "aws_efs_mount_target" "efs_mount_target_a" {
  file_system_id = aws_efs_file_system.wordpress_content.id
  #availability_zone_name = "us-east-1a"
  subnet_id      = "subnet-0e6b310a77b8a6e1c"
  security_groups = ["sg-09e2edcccb8095d62"]
}

resource "aws_efs_mount_target" "efs_mount_target_b" {
  file_system_id = aws_efs_file_system.wordpress_content.id
  #availability_zone_name = "us-east-1b"
  subnet_id      = "subnet-0c012de365fb83478"
  security_groups = ["sg-09e2edcccb8095d62"]
}

resource "aws_efs_mount_target" "efs_mount_target_c" {
  file_system_id = aws_efs_file_system.wordpress_content.id
  #availability_zone_name = "us-east-1c"
  subnet_id      = "subnet-036b762e853087b4f"
  security_groups = ["sg-09e2edcccb8095d62"]
}
```
![](https://lh7-us.googleusercontent.com/FLDoPbDtpv-x70iO-U1II6V9Rk2jgTl5HOcJKEUjP3tr2tu_TWlHHBhEwzxjlGFx2bEn0e6GCAT8eR5o-2IFx37q6JWS4WJlalBbdFF5X0PI9TgTTBdF75oYlxehvdWiBoswsgtXN7BlE_olCiFHC68)

![](https://lh7-us.googleusercontent.com/MIVND_xK0R-U6Djzh6JpP7O8hKDgtzDB1V-Lfw-uEoPyOMjrLarEyE1spccY6LyFzhSr7oFzlOsxuVQLFMQWQEvphdQ5V1DTp0-dKn1ES3XxRRFthD8vRe0nAQq7zH0h7CDDbZYZ6gNPPhsHFxrxA0k)


 5. ### PARAMETERS module. 

 created resources - aws_ssm_parameter. For simplicity, I have hardcoded the value directly inside the main.tf file (Note: this have to be avoided). Checked with Terraform init, plan and apply command. Then logged into the AWS Management console, choose the region to make sure the parameters were created. Reference - [ssm_parameter](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssm_parameter)

 ![](https://lh7-us.googleusercontent.com/AEBr3QUZFPE9nDqEYF34TJ-cqgRk-UYl78coRwbsHBf78skVM9zerf0-95lpQr4fZEE2K_JaJtndTq0LYe5REqyScVot_Pg4SgZb22oPQo14jQaK-4o18grBV8VJ9aZ4AT6zEZvEH2bs8Q1Se1dmiqo)

 ![](https://lh7-us.googleusercontent.com/1RgybMhUHBVfUf740K2ttom_fUuc9IAYOp7iZsxBaUKhpdTKbBbF9eVTl8QrKLZdZQpk74UNs6LouzkK2alADnfWSgHI_LRLTZCnK0uLmHGUwoMlvs8bbaZ7W9T0pfTjNHFDPo0bwlUIThlOrGNSrSE)



 6. ### EC2 module. 
 
 * IAM role named "WPRole" with a specific assume role policy allowing EC2 instances to assume this role. The IAM role is assigned three managed policies: CloudWatchAgentServerPolicy, AmazonSSMFullAccess, and AmazonElasticFileSystemClientFullAccess.

Additionally, an IAM instance profile named "WordpressInstanceProfile" is created and associated with the "WPRole" IAM role. The instance profile is intended to be attached to EC2 instances, providing them with the associated IAM role's permissions.. Reference - [iam_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role)
 [iam_instance_profile](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_instance_profile)

```
resource "aws_iam_role" "wordpress_role" {
  name               = "WPRole"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF

  path = "/"

  managed_policy_arns = [
    "arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy",
    "arn:aws:iam::aws:policy/AmazonSSMFullAccess",
    "arn:aws:iam::aws:policy/AmazonElasticFileSystemClientFullAccess",
  ]
}

# IAM Instance Profile
resource "aws_iam_instance_profile" "wordpress_instance_profile" {
  name = "WordpressInstanceProfile"
  path = "/"

  role = aws_iam_role.wordpress_role.name
}
```

 * Created two security Groups - one for Application Load Balancer and another one for EC2 instance . Reference - [security_group](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group#basic-usage)

```
  resource "aws_security_group" "ec2sg" {
  name        = "ec2sg"
  description = "Allow Inbound Traffic on Port 80"

  ingress {
    description      = "Port 80 from Everywhere"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
 }
  
  resource "aws_security_group" "albsg" {
  name        = "albsg"
  description = "Allow Inbound Traffic on Port 80"

  ingress {
    description      = "Port 80 from Everywhere"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
 }
```

 * AWS Launch Template named "wordpress_launch_template" for launching EC2 instances. It specifies the instance details, such as the Amazon Machine Image (AMI), instance type, security group, IAM instance profile, credit specification, and user data script. The user data script is encoded from a local file named "setup.sh" and is used to configure instances during launch.. This script sets up a WordPress installation on an Amazon EC2 instance, using MariaDB for the database and Amazon EFS for shared content. It retrieves sensitive information from AWS Parameter Store, installs necessary packages, configures the Apache HTTP server, downloads and configures WordPress, sets permissions, and creates a script for updating database URLs. The script is scheduled to run on system startup to ensure the WordPress installation remains updated.  Reference - [aws_launch_template](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/launch_template)
```
resource "aws_launch_template" "wordpress_launch_template" {
  name          = "Wordpress"
  image_id      = "ami-0230bd60aa48260c6"
  instance_type = "t2.micro" 
  vpc_security_group_ids = [aws_security_group.ec2sg.id]
  iam_instance_profile {
    name = aws_iam_instance_profile.wordpress_instance_profile.name
  }
  credit_specification {
    cpu_credits = "standard"
  }
  user_data = base64encode(file("./EC2/setup.sh"))
}
```

 * AWS Auto Scaling Group named "wordpress_asg." It maintains a desired capacity of 1 to 2 EC2 instances, spread across specified subnets. Instances are launched from a specified template, and health is checked using an Elastic Load Balancer. The ASG is tagged with "Name: DemoASG" and can adjust the number of instances based on demand.. Reference - [aws_autoscaling_group](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/autoscaling_group)
```
resource "aws_autoscaling_group" "wordpress_asg" {
  desired_capacity     = 1  
  max_size             = 2  
  min_size             = 1  
  vpc_zone_identifier  = ["subnet-012129ee2dab54ed3","subnet-02237afd05fadf352","subnet-095def75081e7483b"]  # Replace with your subnet ID
  launch_template {
    id      = aws_launch_template.wordpress_launch_template.id
    version = "$Latest"
  }
  health_check_type          = "ELB"
  health_check_grace_period  = 300
  force_delete                = true
  protect_from_scale_in      = false
  tag {
    key                 = "Name"
    value               = "DemoASG"
    propagate_at_launch = true
  }
}
```

 * AWS Application Load Balancer (ALB) named "DEMOALB" specifies three subnets and associates a security group named "albsg" with the ALB. Additionally, it enables features such as HTTP/2, cross-zone load balancing, and disables deletion protection for the ALB. Reference - [ALB](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb)
 ```
resource "aws_lb" "wordpress_alb" {
  name               = "DEMOALB"
  internal           = false
  load_balancer_type = "application"
  enable_deletion_protection = false

  subnets = [
    "subnet-012129ee2dab54ed3",
    "subnet-02237afd05fadf352",
    "subnet-095def75081e7483b"
  ]

  security_groups = [aws_security_group.albsg.id]

  enable_http2             = true
  enable_cross_zone_load_balancing = true
}
```

 * Target Group named "DEMOALBTG" for an Application Load Balancer (ALB). The target group is configured to handle HTTP traffic on port 80 and is associated with a specific VPC. Health checks are enabled, and the ALB checks the health of the registered instances every 30 seconds by sending an HTTP request to the "/" path. The target instances are considered healthy if they respond successfully for two consecutive checks and unhealthy if they fail for two consecutive checks, with a timeout of 5 seconds for each health check. Reference - [lb_target_group](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb_target_group)
```
resource "aws_lb_target_group" "wordpress_alb_target_group" {
  name        = "DEMOALBTG"
  port        = 80
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = "vpc-0c50466ebfe5d9d37"

  health_check {
    enabled             = true
    interval            = 30
    path                = "/"
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 5
  }
}
```
 * The ALB listener is set up to handle HTTP traffic on port 80. The default action for this listener is configured to forward requests to a target group associated with the ALB.The rule is triggered when the requested path matches "/" (root path). When triggered, the rule forwards requests to the same target group as defined in the listener. Reference - [aws_lb_listener_rule](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb_listener_rule)
```
resource "aws_lb_listener" "wordpress_listener" {
  load_balancer_arn = aws_lb.wordpress_alb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "forward"
    target_group_arn = aws_lb_target_group.wordpress_alb_target_group.arn
 #   weight           = 100
    
  }
}

resource "aws_lb_listener_rule" "wordpress_listener_rule" {
  listener_arn = aws_lb_listener.wordpress_listener.arn

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.wordpress_alb_target_group.arn
  }

  condition {
    path_pattern {
      values = ["/"]
    }
  }
}
```
 * Attachment between an AWS Auto Scaling Group (aws_autoscaling_group) and an Application Load Balancer Target Group (aws_lb_target_group). This attachment establishes a connection, allowing instances launched by the Auto Scaling Group to be registered as targets in the specified Target Group of the associated Application Load Balancer. Reference - [aws_autoscaling_attachment](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/autoscaling_attachment)
```
resource "aws_autoscaling_attachment" "wordpress_tg_attachmen" {
  autoscaling_group_name = aws_autoscaling_group.wordpress_asg.id
  lb_target_group_arn    = aws_lb_target_group.wordpress_alb_target_group.arn
}
```
![](https://lh7-us.googleusercontent.com/kAYtgLVNmO3OKT85b6cI4JL0_8RSmRAZD44DqKM-zSwOEWNxqilIHZwBQoDysQcthnTx3StThumqDKHLx2YIsAK82rqh7LYD1rOpC6cqqp8CJF0oo4bfAWvP_bp1uWovNFcUaLLCLr08eXj7-sy3Uyg)

![](https://lh7-us.googleusercontent.com/d0vYPpDa7GsRRunTLvYno8ccTwMtGSNeffxGWq_6FEKBp615RHue_6emTweX6cQtTSWz67ajEAx4hK_TDW2l3L9qpmw-DPjX5dbfv0gCXnyvfViiHx0-wg8aEDfvkbI1Z87Qb86G6HPVNLfGgU011Xs)

![](https://lh7-us.googleusercontent.com/8FkRtIJbAkvTUGr1JAuvGzr4uVQpKbk0j5dtnAt8zGP27bAzPPMpFseQNb9glARaBCfHE9JJTvx1OT5Je1Qoil_7G11mmLU402E7rGRjyEVlYanUC89-gakCVTIrN4ykuVeqYEMFz818nwFJz0HkXr8)

![](https://lh7-us.googleusercontent.com/UXOc-dK49rDKmuFpUjxpZYliC-FDtm_COlEsxe7N6I4oEXAPu5pISFGdUPlOP5W-HFImQOyVTNJsSXDqkkpvpecMcYpusScgOhWg5lOSk5TtXJN6AzeqbZrYkjM-hEA_fvlZJGKIYEZ78yx0DPUGp0c)


Terraform Commands used to deploy all the above resources are given below.

Initalizing by using 
```
terraform init
```
Plan the changes that will be applied by this configuration
```
terraform plan
```
Apply the planned changes and create resources.
```
terraform apply -auto-approve
```
Delete the resources to save the money
```
terraform destroy -auto-approve
```
