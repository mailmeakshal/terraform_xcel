terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "5.26.0"
    }
  }
  required_version = ">=1.0.0"
}

provider "aws" {
  region     = "us-east-1"
}

module "EC2" {
  source         = "./EC2"
}

module "PARAMETERS" {
  source         = "./PARAMETERS"
}

module "RDS" {
 source         = "./RDS"
}

module "EFS" {
  source         = "./EFS"
}